Tech Test

Task
User should be able to add an item to the basket and verify the correct item is added

Notes
Create simple automation framework
May use any online purchasing platform e.g. Amazon, Takealot, Woolworths
No need to automate signing into purchasing platform
May use any programming language
Test executes
One hour to complete
