import org.testng.annotations.AfterTest;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddItemTest {
	WebDriver driver;
	String baseUrl ;
	private String itemName = "purity peach";
	
	@BeforeTest
	public void setBrowser() {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Admin\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://www.takealot.com/";
	}
	
	@Test(priority=0)
	public void searchAnItem() {
		driver.get(baseUrl);
		driver.manage().window().maximize();
		WebElement idEle = driver.findElement(By.name("search"));
		idEle.sendKeys(itemName);
		idEle.sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS) ;
		String winHandleBefore = driver.getWindowHandle();
		System.out.println("winHandleBefore: "+winHandleBefore);
		WebElement itemResult = driver.findElement(By.cssSelector("#\\35 4551847 > div > a"));
		itemResult.click();
		for(String winHandle : driver.getWindowHandles()){
			System.out.println("winHandle: "+winHandle);
			if( winHandle!= winHandleBefore) {
		    driver.switchTo().window(winHandle);
			}
		}
		String winHandleAfter = driver.getWindowHandle();
		System.out.println("winHandleAfter: "+winHandleAfter);
		driver.findElement(By.linkText("Add to Cart")).click();
		driver.findElement(By.cssSelector("#shopfront-app > div:nth-child(1) > div > div > header > div.grid-container.fluid.main-strip > div > div.auto.cell.mobile-header-module_right-icons-container_HvSLW > div.mobile-header-module_cart-icon-container_1WVBw > a")).click();
	}

	@AfterTest
	public void closeBrowser() {
		driver.close();
	}
	

}
